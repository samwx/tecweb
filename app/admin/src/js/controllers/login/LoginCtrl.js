angular
  .module('twb')
  .controller('LoginController', function LoginController($rootScope, $scope, $window, $cookies, authService) {
    'use strict';

    var controller = this;

    controller.password = '';
    controller.email = authService.user.email;
    controller.remeber = false;

    controller.isLoading = function() {
      return authService.isWorking();
    };

    controller.login = function($event) {

      $event.preventDefault();

      authService
        .login(controller.email, controller.password)
        .then(function(userInfo) {
          const token = userInfo.data.token;

          if( controller.remember == true ) {
            $cookies.put('userToken', token);
          }
          else {
            $rootScope.userToken = token;
          }

          $window.location.href = '/admin/#/';
        });
    };
  });
