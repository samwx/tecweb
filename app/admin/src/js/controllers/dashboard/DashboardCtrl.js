angular
  .module('twb')
  .controller('DashboardCtrl', function($scope, $rootScope, RestService, notificationService){
    var controller = this;

    /* User REST routes */
    $scope.restUser = new RestService({
      path: '/user'
    });

    /* Services REST routes */
    $scope.restServices = new RestService({
      path: '/services'
    });

    /* Users REST routes */
    $scope.restUsers = new RestService({
      path: '/users'
    });

    /* Total users */
    $scope.restUser
      .get('/total')
      .then(function(resp){
        controller.totalUsers = resp.data.data;
      });

    /* Total services */
    $scope.restServices
      .get('/total')
      .then(function(resp){
        controller.totalServices = resp.data.data;
      });

    /* Best services */
    $scope.restServices
      .get('/best/5')
      .then(function(resp){
        $scope.bestServices = resp.data.data;
      });

    /* Worse services */
    $scope.restServices
      .get('/worse/5')
      .then(function(resp){
        $scope.worseServices = resp.data.data;
      });

    /* Unapproved providers */
    controller.updateUnapprovedProviders = function(){
      $scope.restUsers
        .get('/providers/unapproved')
        .then(function(resp){
          $scope.unapprovedProviders = resp.data.data;
        });
    };
    controller.updateUnapprovedProviders();

    /* Unapproved customers */
    controller.updateUnapprovedCustomers = function(){
      $scope.restUsers
        .get('/customers/unapproved')
        .then(function(resp){
          $scope.unapprovedCustomers = resp.data.data;
        });
    }
    controller.updateUnapprovedCustomers();

    controller.approveUser = function(userId, type){
      console.log(userId);
      console.log(type);

      $scope.restUser
        .put('/approve/' + userId)
        .then(function(resp){
          if ( type == 'provider') {
            notificationService.success('Prestador aprovado!', 'SUCCESS');
            controller.updateUnapprovedProviders();
          }
          else {
            notificationService.success('Cliente aprovado!', 'SUCCESS');
            controller.updateUnapprovedCustomers();
          }
        });
    }
  });
