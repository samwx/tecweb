angular
  .module('twb')
  .controller('SearchServicesCtrl', function($scope, $rootScope, RestService){
    var controller = this;

    $scope.restCategories = new RestService({
      path: '/categories'
    });

    $scope.restServices = new RestService({
      path: '/services'
    });

    $scope.restCategories
      .get('/')
      .then(function(resp){
        $scope.categories = resp.data.data
      });

    controller.search = function(name, category, rating) {
      var rating = (rating) ? rating : 0;

      $scope.restServices
      .get('/search/' + name + '/' + category + '/'+ rating)
      .then(function(resp){
        $scope.searchMessage = 'Resultados da busca por: ' + name;
        $scope.searchResults = resp.data.data;
      });
    }
  });
