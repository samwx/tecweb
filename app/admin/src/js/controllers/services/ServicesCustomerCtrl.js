angular
  .module('twb')
  .controller('ServicesCustomerCtrl', function($scope, $rootScope, RestService, notificationService){
    var controller = this;
    $scope.restService = new RestService({
      path: '/services'
    });

    controller.rating = [{value:0}, {value:1}, {value:2}, {value:3}, {value:4}, {value:5}];
    $scope.totalValue = 0;

    $scope.restService
      .get('/bycustomerandstatus/' + $rootScope.userLoggedId + '/realized')
      .then(function(resp){
        $scope.realizedServices = resp.data.data;
        $scope.realizedServices.forEach(function(item){
          $scope.totalValue += item.value;
        });
      });

    $scope.rateService = function(service){
      Date.prototype.toYMD = function() {
        var year, month, day;
        year = String(this.getFullYear());
        month = String(this.getMonth() + 1);

        if (month.length == 1) month = "0" + month;

        day = String(this.getDate());
        if (day.length == 1) day = "0" + day;

        return year + "-" + month + "-" + day;
      }

      var today = new Date();
      service.rating = controller.selectedRate;
      service.realizedAt = today.toYMD();

      $scope.restService
        .put('/update/', service)
        .then(function(resp){
          console.log(resp);
          notificationService.success('Serviço avaliado', 'SUCCESS');
        });
    }
  });
