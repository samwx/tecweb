angular
  .module('twb')
  .controller('RegisterServiceCtrl', function($scope, $rootScope, RestService, notificationService){
    var controller = this;

    $scope.restCategories = new RestService({
      path: '/categories'
    });

    $scope.restServices = new RestService({
      path: '/services'
    });

    $scope.restCategories
      .get('/')
      .then(function(resp){
        console.log(resp.data.data);
        $scope.categories = resp.data.data
      });

    controller.register = function($event){
      controller.field['provider'] = {"id": $rootScope.userLoggedId};
      controller.field['category'] = JSON.parse(controller.field['category']);
      controller.field['value'] = parseFloat(controller.field['value']);
      var data = controller.field;

      console.log(JSON.stringify(data));
      $scope.restServices
        .post('/create', JSON.stringify(data))
        .then(function(resp){
          notificationService.success('Serviço cadastrado com sucesso!', 'SUCCESS');
          console.log(resp);
        });
    }
  });
