angular
  .module('twb')
  .controller('ServiceCtrl', function($scope, $rootScope, $routeParams, RestService, notificationService){
    $scope.restService = new RestService({
      path: '/services'
    });

    $scope.restService
      .get('/' + $routeParams.id)
      .then(function(resp){
        console.log(resp);
        $scope.service = resp.data.data;
      });

    $scope.hire = function(service){
      var myDate = new Date();
      var myDate_string = myDate.toISOString();
      var myDate_string = myDate_string.replace("T"," ");
      var myDate_string = myDate_string.substring(0, myDate_string.length - 5);

      service.customer = {id: parseInt($rootScope.userLoggedId)};
      service.status = 'realized';

      $scope.restService
        .put('/update', service)
        .then(function(resp){
          notificationService.success('Serviço contratado', 'SUCCESS');
        });
    }
  });
