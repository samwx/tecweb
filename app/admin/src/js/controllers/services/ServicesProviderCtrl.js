angular
  .module('twb')
  .controller('ServicesProviderCtrl', function($scope, $rootScope, RestService){
    var controller = this;
    $scope.restService = new RestService({
      path: '/services'
    });

    $scope.totalValue = 0;

    $scope.restService
      .get('/byproviderandstatus/' + $rootScope.userLoggedId + '/realized')
      .then(function(resp){
        console.log(resp);
        $scope.realizedServices = resp.data.data;
        $scope.realizedServices.forEach(function(item){
          $scope.totalValue += item.value;
        });
      });
  });
