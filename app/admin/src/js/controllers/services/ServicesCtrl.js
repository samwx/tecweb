angular
  .module('twb')
  .controller('ServicesCtrl', function($scope, $rootScope, RestService){
    /* Services REST routes */
    $scope.restServices = new RestService({
      path: '/services'
    });

    /* Best services */
    $scope.restServices
      .get('/status/realized')
      .then(function(resp){
        $scope.realizedServices = resp.data.data;
      });
  });
