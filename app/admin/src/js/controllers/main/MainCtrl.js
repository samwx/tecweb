angular
  .module('twb')
  .controller('MainCtrl', function($scope, $rootScope, authService){
    $scope.servicesMenu = function(){
      if($rootScope.userAccessRole == 'customer') {
        return '#/services-customer';
      }
      if($rootScope.userAccessRole == 'provider') {
        return '#/services-provider';
      }
      if($rootScope.userAccessRole == 'admin') {
        return '#/services';
      }
    };

    $scope.logout = function(){
      console.log('logout');
      authService.logout();
    };

    $scope.isAdmin = function(){
      return $rootScope.userAccessRole == 'admin';
    };

    $scope.isProvider = function(){
      return $rootScope.userAccessRole == 'provider';
    };

    $scope.isCustomer = function(){
      return $rootScope.userAccessRole == 'customer';
    };
  });
