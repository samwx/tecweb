angular
  .module('twb')
  .controller('ReportsCtrl', function($scope, $rootScope, RestService){
    /* Services REST routes */
    $scope.restServices = new RestService({
      path: '/services'
    });

    /* Best services */
    $scope.restServices
      .get('/best/5')
      .then(function(resp){
        $scope.bestServices = resp.data.data;
      });

    /* Worse services */
    $scope.restServices
      .get('/worse/5')
      .then(function(resp){
        $scope.worseServices = resp.data.data;
      });

    $scope.filterByMonth = function(value){
      if ( value && value.length == 1 ) {
        value = '0' + value;

        return function(item){
          return item.realizedAt.split('-')[1] == value;
        }
      }
    };
  });
