//Main module name
var app = angular.module('twb', [
  'ngRoute',
  'twb.notification',
  'ngCookies'
]);

// App bootstrap configuration constants
app.constant('$frontendUrl', 'http://localhost:3000');
app.constant('$backendUrl', 'http://localhost:8080');

// App bootstrap configuration
app
.config(function($routeProvider, $httpProvider){
  $routeProvider
    .when('/', {
      access: 'admin',
      templateUrl: 'src/views/dashboard.html'
    })
    .when('/search', {
      templateUrl: 'src/views/search.html'
    })
    .when('/services', {
      templateUrl: 'src/views/services.html'
    })
    .when('/services-customer', {
      templateUrl: 'src/views/services-customer.html'
    })
    .when('/services-provider', {
      templateUrl: 'src/views/services-provider.html'
    })
    .when('/service/:id', {
      templateUrl: 'src/views/service.html'
    })
    .when('/reports', {
      templateUrl: 'src/views/reports.html'
    })
    .when('/new-service', {
      templateUrl: 'src/views/new-service.html'
    })
    .otherwise('/');
});

// App bootstrap initialization
app.run(function($rootScope, tokenService, $window, $location, authService) {
  var token = tokenService;

  authService.load(token);
});
