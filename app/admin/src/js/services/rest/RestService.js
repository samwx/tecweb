angular
  .module('twb')
  .provider('RestService', function RestServiceProvider() {
    'use strict';

    var providerConfig = {
      url: 'http://localhost:8080/api',
      options: {}
    };

    // Provider configuration
    this.setBaseUrl = function(url) {
      providerConfig.url = url;
    };

    this.getBaseUrl = function() {
      return providerConfig.url;
    };

    // Service initialization
    this.$get = function($http, DeferService, notificationService, tokenService) {

      return $class(DeferService, {

        constructor: function(config) {

          DeferService.call(this);

          this.baseUrl = (config.url || providerConfig.url) + (config.path || '');
        },

        toUrl: function(path) {
          return this.baseUrl + (path || '');
        },

        _notifySuccess: function(response) {
          // dont use this.. callback only
          if (response && response.messages) {
            notificationService.push(response.messages);
          }
          // return response, to other callbacks registered
          return response;
        },
        _notifyError: function(response) {
          var notified = false;
          // dont use this.. callback only
          if (response) {
            if (response.messages) {
              notified = true;
              notificationService.push(response.messages);
            }
          }
          if (!notified) {
            notificationService.push('Ops, nÃ£o consegui executar a operação. Verifique os dados e tente novamente.');
          }
          // return response, to other callbacks registered
          return response;
        },

        get: function(path, config) {
          var token = tokenService;

          var config = config || { headers: { Authorization: 'Bearer ' + token } };

          return this.defer($http.get(this.toUrl(path), config))
            .success(this._notifySuccess)
            .error(this._notifyError);
        },

        post: function(path, data, config) {
          var token = tokenService;

          var config = config || { headers: { Authorization: 'Bearer ' + token } };
          return this.defer($http.post(this.toUrl(path), data, config))
            .success(this._notifySuccess)
            .error(this._notifyError);
        },

        put: function(path, data, config) {
          var token = tokenService;

          var config = config || { headers: { Authorization: 'Bearer ' + token } };
          return this.defer($http.put(this.toUrl(path), data, config))
            .success(this._notifySuccess)
            .error(this._notifyError);
        },

        delete: function(path, config) {
          var token = tokenService;

          var config = config || { headers: { Authorization: 'Bearer ' + token } };
          return this.defer($http.delete(this.toUrl(path), config))
            .success(this._notifySuccess)
            .error(this._notifyError);
        }
      });
    };
  });
