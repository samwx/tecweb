angular
  .module('twb.notification', ['ui-notification'])
  .factory('notificationService', function notificationServiceFactory(Notification) {
    'use strict';

    var notifications = [];

    return {

      push: function(content) {
        if (content) {
          if (content.SUCCESS) {
            this.pushMessages(content.SUCCESS);
          }
          if (content.INFO) {
            this.pushMessages(content.INFO);
          }
          if (content.WARNING) {
            this.pushMessages(content.WARNING);
          }
          if (content.ERROR) {
            this.pushMessages(content.ERROR);
          }
        }
      },

      pushMessages: function(messages) {
        for (var i = 0; i < messages.length; i++) {
          this.pushMessage(messages[i]);
        }
      },

      pushMessage: function(msg, type) {

        var text = (msg.message || msg.key || msg);

        type = (type || msg.type).toUpperCase();

        if (type === 'ERROR') {
          this.error(text);
        } else if (type === 'WARNING') {
          this.warning(text);
        } else if (type === 'SUCCESS') {
          this.success(text);
        } else {
          this.info(text);
        }
        // remove last
        this.addNotification(text, type);
      },

      addNotification: function(text, type) {
        if (notifications.length > 15) {
          notifications.splice(0, 1);
        }
        notifications.push({
          type: type,
          text: text
        });
      },

      getNotifications: function(){
        return notifications;
      },
      error: function(msg) {
        Notification.error(msg);
      },
      warning: function(msg) {
        Notification.warning(msg);
      },
      success: function(msg) {
        Notification.success(msg);
      },
      info: function(msg) {
        Notification.info(msg);
      }
    };
  })
  .config(function(NotificationProvider) {
    NotificationProvider.setOptions({
      delay: 3 * 1000,
      startTop: 60,
      startRight: 20,
      verticalSpacing: 10,
      horizontalSpacing: 10,
      positionX: 'right',
      positionY: 'top'
    });
  });
