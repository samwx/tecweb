angular
  .module('twb')
  .factory('tokenService', function TokenServiceFactory($rootScope, $cookies){
    'use strict';

    if ( $cookies.get('userToken') || $rootScope.userToken )
      this.token = $cookies.get('userToken') || $rootScope.userToken;
    else
      this.token = null;

    return this.token;
  });
