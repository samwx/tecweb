angular
  .module('twb')
  .factory('authService', function AuthServiceFactory($rootScope, $cookies, $window, $location, DeferService, RestService) {
    'use strict';

    var AuthService = $class(DeferService, {

      constructor: function() {

        DeferService.call(this);

        this.user = false;
        this.loaded = false;

        this.rest = new RestService({
          path: '/auth'
        });
      },

      _httpBind: function(http, ok, error) {
        // http already have deferWork from DeferService
        return http.then(angular.bind(this, ok), angular.bind(this, error));
      },

      check: function(token) {

        var deferred = this.defer();
        var auth = this;

        this._httpBind(
          this.rest.get('/user',{
            headers: {
              Authorization: 'Bearer ' + token
            }
          }),
          function(response) {
            $rootScope.userFullName = response.data.data.name;
            $rootScope.userLoggedId = response.data.data.id;
            $rootScope.userAccessRole = response.data.data.access;

            var user = response && response.data;
            if (user) {
              // copy internal
              this.user = angular.copy(user, {});
              auth.user = user;
              // ok
              deferred.resolve(user);
            } else {
              deferred.reject(this.user = false);
            }
          },
          function() {
            deferred.reject(this.user = false);
          }
        );

        return deferred.promise;
      },

      login: function(email, password) {

        var deferred = this.defer();

        this._httpBind(
          this.rest.post('/login', {
            login: email,
            password: password
          }, {}),
          function(response) {
            var user = response && response.data;
            if (user) {
              // copy internal
              this.user = angular.copy(user, {});
              // ok
              deferred.resolve(user);
            } else {
              deferred.reject(this.user = false);
            }
          },
          function() {
            deferred.reject(this.user = false);
          }
        );

        return deferred.promise;
      },

      logout: function(tokenService) {
        /**
         *
         * MUDAR!
         *
         */

        $cookies.put('userToken', null);
        $rootScope.userToken = null;

        var loginPage = 'login.html';
        $window.location.href = loginPage;
      },

      isLogged: function() {
        return this.user !== false;
      },

      getUsername: function() {
        return this.user ? this.user.username : null;
      },

      getUserInfo: function() {
        return this.user;
      },

      getRole: function(){
        return this.user;
      },

      hasRole: function(role) {
        if($rootScope.userAccessRole != role && role != 'public')
          return false;

        return true;
      },

      load: function(token) {
        if (this.loaded) {
          return;
        }

        var auth = this;
        var loginPage = 'login.html';
        var currentPage = $window.location.toString();
        var adminPage = '/admin/#/';

        // execute auth check and fire events LOGIN
        auth.check(token)
          .then(function(userInfo) {
            // login ok
            if ( userInfo.data.approved ) {
              if (currentPage.indexOf(loginPage) > 0) {
                $window.location.href = adminPage;
              }
            }
            else {
              $window.location.href = loginPage;
            }
          }, function() {
            // not logged
            if (currentPage.indexOf(loginPage) === -1) {
              $window.location.href = loginPage;
            }
          }).finally(function() {
            auth.loaded = true;
          });

        // Cada troca de view faz checagem de permissao.
        $rootScope.$on('$routeChangeStart', function(event, nextRoute, currRoute) {

          var access = (nextRoute && nextRoute.access) || 'public';

          if (!auth.hasRole(access)) {
            if (!auth.isLogged()) {
              //$window.location.href = 'login.html';
              //console.log('user not authenticated');
            } else {
              $window.location.href = '#/services';
              console.log('user has no access role');
            }
          }
        });
      }
    });

    return new AuthService();
  });
