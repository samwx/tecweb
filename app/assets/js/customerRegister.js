$(function(){
  function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
      indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
  }

  const registerUrl = 'http://localhost:8080/api/users/customers/create';

  /**
   *
   * Register client
   *
   */

  $('#customerRegister').on('submit', function(){
    var form = $(this);
    var formData = JSON.stringify(getFormData(form));
    console.log(formData);

    $.ajax({
      url: registerUrl,
      crossDomain: true,
      method: 'POST',
      contentType: 'application/json',
      dataType: 'json',
      data: formData
    }).success(function(response){
      console.log(response);
    });

    return false;
  });
});
