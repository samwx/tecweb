$(function(){
  /**
   *
   * Get Random services from API
   *
   */
  const apiUrl = 'http://localhost:8080/api/services/randomservices/4';
  var content = ''

  $.ajax({
    url: apiUrl,
    crossDomain: true,
    method: 'GET'
  }).success(function(resp){
    resp.data.forEach(function(item){
      content += '<div class="col-sm-3"><div class="box-model-1"><div class="caption"><h3>' + item.name + '</h3><p>'+ item.description +'</p></div></div></div>';
    });

    $('#randomServices').html(content);
  });

});
