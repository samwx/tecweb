$(function(){
  function cleanForm() {
    $('input[name="cep"], input[name="address"]').val();
  }

  $("#cep").blur(function() {

    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if(validacep.test(cep)) {
        //Preenche os campos com "..." enquanto consulta webservice.
        $("#address").val("...");

        //Consulta o webservice viacep.com.br/
        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
          if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $("#address").val(dados.logradouro + ', ' + dados.bairro + ', ' + dados.localidade + ', ' + dados.uf);
          } //end if.
          else {
            //CEP pesquisado não foi encontrado.
            cleanForm();
            alert("CEP não encontrado.");
          }
        });
      }
      else {
        //cep é inválido.
        cleanForm();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      cleanForm();
    }
  });
});
